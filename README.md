# Dépôt principal du Projet OGSpy #

Le projet crée en 2006 est une aide pour un jeu de gestion de vaisseaux spaciaux.

Le but de cet outil est de récupérer l'ensemble des informations du Jeu pour ensuite les regrouper et les exploiter.

### Que contient ce dépôt ? ###

* OGSpy Version 3.3.1
* Librairies externes comme JQuery
* Accord de License

### Comment installer OGSpy ###

Nous avons réalisé un wiki qui détaille l'installation de l'outil sur un serveur web. Mais il existe aussi des hébergeurs qui installent le site pour vous.

Vous touverez toutes ces informations sur le wiki : 

[Wiki de l'OGSteam](http://wiki.ogsteam.fr/doku.php)

### Pour contribuer au projet ###

* Vous pouvez nous aider sur le développement
* Nous avons besoins de vos idées pour améliorer l'outil
* Des volontaires pour la documentations dans toutes les langues connues
* Traductions

Vous pouvez nous poser toutes les questions nécessaires sur notre forum.

### Contributeurs au projet ###

Responsable équipe : [DarkNoon](https://bitbucket.org/darknoon29)

####Développement####

* Machine
* Jedinight
* Superbox 
* Pitch314
* Shad
* Xaviernuma
* Ninety

####Tests####

* Vous tous !!

####Graphismes####

 Chris Alys 

### Pour nous contacter ###

Notre forum : [Forum de l'OGSteam](http://forum.ogsteam.fr)