<?php
/**
 * Main menu
 * @package OGSpy
 * @version 3.04b ($Rev: 7508 $)
 * @subpackage views
 * @author Kyser
 * @created 15/12/2005
 * @copyright Copyright &copy; 2007, http://ogsteam.fr/
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

if (!defined('IN_SPYOGAME')) {
    die("Hacking attempt");
}

?>



<div>
            <script type="text/javascript">
                var date = new Date;
                var delta = Math.round((<?php echo (time() * 1000);?> -date.getTime()) / 1000);
                function Timer() {
                    var days = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"];
                    var months = ["Jan", "Fév", "Mar", "Avr", "Mai", "Jui", "Jui", "Aoû", "Sep", "oct", "nov", "déc"];

                    date = new Date;
                    date.setTime(date.getTime() + delta * 1000);
                    var hour = date.getHours();
                    var min = date.getMinutes();
                    var sec = date.getSeconds();
                    var day = days[date.getDay()];
                    var day_number = date.getDate();
                    var month = months[date.getMonth()];
                    if (sec < 10) sec = "0" + sec;
                    if (min < 10) min = "0" + min;
                    if (hour < 10) hour = "0" + hour;

                    var datetime = day + " " + day_number + " " + month + " " + hour + ":" + min + ":" + sec;

                    if (document.getElementById) {
                        document.getElementById("datetime").innerHTML = datetime;
                    }
                }

                go_visibility = [];
                function goblink() {
                    if (document.getElementById && document.all) {
                        var blink_tab = document.getElementsByTagName('blink');
                        for (var a = 0; a < blink_tab.length; a++) {
                            if (go_visibility[a] != "visible")
                                go_visibility[a] = "visible";
                            else
                                go_visibility[a] = "hidden";
                            blink_tab[a].style.visibility = go_visibility[a];
                        }
                    }
                }

                function Biper() {
                    Timer();
                    goblink();

                    setTimeout("Biper()", 1000);
                }

                window.onload = Biper;
            </script>
            <!-- Navigation -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <b><?php echo($lang['MENU_SERVER_TIME']); ?></b><br/>
                            <span id="datetime"><blink><?php echo($lang['MENU_WAITING']); ?></blink></span>
                            <div><a href="index.php"><img src="./skin/Kaoskin/images/logo.png" width="166" height="65" border="0"/></a></div>
                            <?php

                            if ($server_config["server_active"] == 0) {
                                echo "<tr>\n";
                                echo "\t" . "<div align='center'><font color='red'><b><blink>".$lang['MENU_SERVER_OFFLINE']."</blink></b></font></div>\n";
                                echo "</tr>\n";
                            }

                            ?>
                        </li>
                        <?php
                        if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1 || $user_data["management_user"] == 1) {
                            echo "<li><a href='index.php?action=administration'>".$lang['MENU_ADMIN']."</a></li>";
                        }
                        ?>
                        <li><a href='index.php?action=profile'><?php echo($lang['MENU_PROFILE']); ?></a></li>
                        <li><a href='index.php?action=home'><?php echo($lang['MENU_HOME']); ?></a></li>
                        <li><a href='index.php?action=galaxy'><?php echo($lang['MENU_GALAXY']); ?></a></li>
                        <li><a href='index.php?action=cartography'><?php echo($lang['MENU_ALLIANCES']); ?></a></li>
                        <li><a href='index.php?action=search'><?php echo($lang['MENU_RESEARCH']); ?></a></li>
                        <li><a href='index.php?action=ranking'><?php echo($lang['MENU_RANKINGS']); ?></a></li>
                        <li><a href='index.php?action=statistic'><?php echo($lang['MENU_UPDATE_STATUS']); ?></a></li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> <?php echo($lang['MENU_MODULES']); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <?php
                                     $request = "SELECT action, menu FROM " . TABLE_MOD . " WHERE active = 1 AND `admin_only` = '0' order by position, title";
                                     $result = $db->sql_query($request);

                                     if ($db->sql_numrows($result)) {
                                         while ($val = $db->sql_fetch_assoc($result)) {
                                             echo '<li><a href="index.php?action=' . $val['action'] . '">' . $val['menu'] . '</a></li>';
                                         }
                                     }

                                    if ($user_data["user_admin"] == 1 || $user_data["user_coadmin"] == 1) {
                                        $request = "SELECT action, menu FROM " . TABLE_MOD . " WHERE active = 1 and `admin_only` = '1' order by position, title";
                                        $result = $db->sql_query($request);

                                        if ($db->sql_numrows($result)) {
                                            while ($val = $db->sql_fetch_assoc($result)) {
                                                echo '<li><a href="index.php?action=' . $val['action'] . '">' . $val['menu'] . '</a></li>';
                                            }
                                        }
                                    }

                                ?>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php
                            if ($server_config["url_forum"] != "") {
                                echo "<li><a href='" . $server_config["url_forum"] . "'>".$lang['MENU_FORUM']."</a></li>";
                            }
                        ?>
                        <li><a href="index.php?action=about"><?php echo($lang['MENU_ABOUT']); ?></a></li>
                        <li><a href='index.php?action=logout'><?php echo($lang['MENU_LOGOUT']); ?></a></li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
</div>

<!--<script>$( "#menu" ).menu();</script> (Encore pas mal de travail pour mettre ce menu en place) -->