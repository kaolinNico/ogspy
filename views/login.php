<span style="color: red;">A retirer après dév : adp56sd12</span>
<?php
/**
 * Page Login
 * @package OGSpy
 * @version 3.04b ($Rev: 7508 $)
 * @subpackage views
 * @author Kyser
 * @created 15/12/2005
 * @copyright Copyright &copy; 2007, http://ogsteam.fr/
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

if (!defined('IN_SPYOGAME')) {
    die("Hacking attempt");
}


if (!isset($goto)) {
    $goto = "";
}

$enable_register_view = isset ($server_config['enable_register_view']) ? $server_config['enable_register_view'] : 0;

?>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="skin/Skinao/build/css/login.css">
<!--===============================================================================================-->
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
<link rel="icon" type="image/icon" href="favicon.ico"/>
<!--===============================================================================================-->

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="skin/Skinao/build/images/logo.png" alt="Logo">
                    <a href="http://www.ogsteam.fr">OGSpy</a> is an <strong>OGSteam Software</strong><p> &copy;
                    2005-2016</p>
                <span>v <?php echo $server_config["version"];?></span>
            </div>

            <form class="login100-form validate-form" method='post' action=''>
                <input type='hidden' name='action' value='login_web'/>
                <input type='hidden' name='goto' value='<?php echo $goto; ?>'/>

                <div class="wrap-input100 validate-input">
                    <input class="input100" type="text" name="login" placeholder="<?php echo($lang['LOGIN_USER']); ?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "Password is required">
                    <input class="input100" type="password" name="password" placeholder="<?php echo($lang['LOGIN_PASSWORD']); ?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn" type="submit">
                        <?php echo($lang['LOGIN_CONNEXION_BUTTON']); ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--===============================================================================================-->
<script src="skin/Skinao/vendors/jquery/dist/jquery.min.js"></script>
<script src="skin/Skinao/vendors/tilt/tilt.jquery.min.js"></script>
<script >$('.js-tilt').tilt({scale: 1.1})</script>


