<?php
/**
 * Panneau administration des options server
 * @package OGSpy
 * @version 3.04b ($Rev: 7508 $)
 * @subpackage views
 * @author Kyzer
 * @created 07/04/2007
 * @copyright Copyright &copy; 2007, http://ogsteam.fr/
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

if (!defined('IN_SPYOGAME')) {
    die("Hacking attempt");
}

if ($user_data["user_admin"] != 1 && $user_data["user_coadmin"] != 1) {
    redirection("index.php?action=message&amp;id_message=forbidden&amp;info");
}

//Statistiques concernant la base de données
$db_size_info = db_size_info();
if ($db_size_info["Server"] == $db_size_info["Total"]) {
    $db_size_info = $db_size_info["Server"];
} else {
    $db_size_info = $db_size_info["Server"] . " sur " . $db_size_info["Total"];
}

//Statistiques concernant les fichiers journal
$log_size_info = log_size_info();
$log_size_info = $log_size_info["size"] . " " . $log_size_info["type"];

//Statistisques concernant l'univers
$galaxy_statistic = galaxy_statistic();

//Statistics concernant les membres
$users_info = sizeof(user_statistic());

//Statistiques du serveur
$connection_server = 0;
$planetimport_ogs = 0;
$planetexport_ogs = 0;
$spyimport_ogs = 0;
$spyexport_ogs = 0;
$rankimport_ogs = 0;
$rankexport_ogs = 0;
$key = 'unknow';
$paths = 'unknow';
$since = 0;
$nb_users = 0;
$og_uni = 'unknow';
$og_pays = 'unknow';

$request = "select statistic_name, statistic_value from " . TABLE_STATISTIC;
$result = $db->sql_query($request);

while (list($statistic_name, $statistic_value) = $db->sql_fetch_row($result)) {

    switch ($statistic_name) {
        case "connection_server":
            $connection_server = $statistic_value;
            break;

        case "planetimport_ogs":
            $planetimport_ogs = $statistic_value;
            break;

        case "planetexport_ogs":
            $planetexport_ogs = $statistic_value;
            break;

        case "spyimport_ogs":
            $spyimport_ogs = $statistic_value;
            break;

        case "spyexport_ogs":
            $spyexport_ogs = $statistic_value;
            break;

        case "rankimport_ogs":
            $rankimport_ogs = $statistic_value;
            break;

        case "rankexport_ogs":
            $rankexport_ogs = $statistic_value;
            break;
    }
}

//on compte le nombre de personnes en ligne
$connectes_req = $db->sql_query("SELECT COUNT(session_ip) FROM " .
    TABLE_SESSIONS);
list($connectes) = $db->sql_fetch_row($connectes_req);

//Personne en ligne
$online = session_whois_online();

?>

<div class="row">
    <div class="col-md-6">
        <table class="table  table-bordered tableAdmin">
            <colgroup>
                <col class="firstCol">
                <col>
            </colgroup>
            <thead>
                <th><?= $lang['ADMIN_SERVER_STATS']; ?></th>
                <th><?= $lang['ADMIN_SERVER_STATS_VALUE']; ?></th>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_MEMBERS']); ?></td>
                    <td><?php echo $users_info; ?></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_NBPLANETS']); ?></td>
                    <td><?php echo formate_number($galaxy_statistic["nb_planets"]); ?></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_LOG_SIZE']); ?></td>
                    <td><?php echo $log_size_info; ?></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_SESSIONS']); ?></td>
                    <td><?php echo $connectes; ?><a href="index.php?action=drop_sessions"> (<?php echo($lang['ADMIN_SERVER_SESSIONS_CLEAN']); ?> <?php echo help("drop_sessions"); ?>)</td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_CONNEXIONS']); ?></td>
                    <td><?php echo formate_number($connection_server); ?></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_SPYREPORTS']); ?></td>
                    <td><?php echo formate_number($spyimport_ogs)." ".$lang['ADMIN_SERVER_ALL_IMPORT']." - ".formate_number($spyexport_ogs)." ".$lang['ADMIN_SERVER_ALL_EXPORT']; ?></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_PHPINFO']); ?></td>
                    <td><a href="php/phpinfo.php" target="_blank"><?php echo($lang['ADMIN_SERVER_PHPINFO']); ?></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table  table-bordered tableAdmin">
            <colgroup>
                <col class="firstCol">
                <col>
            </colgroup>
            <thead>
                <th><?= $lang['ADMIN_SERVER_STATS']; ?></th>
                <th><?= $lang['ADMIN_SERVER_STATS_VALUE']; ?></th>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_FREEPLANETS']); ?></td>
                    <td><?php echo formate_number($galaxy_statistic["nb_planets_free"]); ?></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_DB_SIZE']); ?></td>
                    <td><?php echo $db_size_info; ?></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_DB_OPTIMIZE']); ?></td>
                    <td><a href="index.php?action=db_optimize"><i><?php echo($lang['ADMIN_SERVER_DB_OPTIMIZE']); ?></i></a></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_PLANETS']); ?></td>
                    <td><?php echo formate_number($planetimport_ogs); ?> <?php echo($lang['ADMIN_SERVER_ALL_IMPORT']); ?>
            - <?php echo formate_number($planetexport_ogs); ?> <?php echo($lang['ADMIN_SERVER_ALL_EXPORT']); ?></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_RANKINGS']); ?></td>
                    <td><?php echo formate_number($rankimport_ogs); ?> <?php echo($lang['ADMIN_SERVER_ALL_IMPORT']); ?> - <?php echo formate_number($rankexport_ogs); ?> <?php echo($lang['ADMIN_SERVER_ALL_EXPORT']); ?></td>
                </tr>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_PHPMODULES']); ?></td>
                    <td><a href="php/phpmodules.php" target="_blank"><?php echo($lang['ADMIN_SERVER_PHPMODULES']); ?></a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table  table-bordered tableAdmin">
            <colgroup>
                <col class="firstCol">
                <col>
            </colgroup>
            <tbody>
                <tr>
                    <td><?php echo($lang['ADMIN_SERVER_INFOVERSION']); ?></td>
                    <td><?php echo($lang['ADMIN_SERVER_INFOVERSION_DESC']); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table tableMembers">
            <thead>
                <th><?php echo($lang['ADMIN_SERVER_MEMBERNAME']); ?></th>
                <th><?php echo($lang['ADMIN_SERVER_MEMBERCONNECTED']); ?></th>
                <th><?php echo($lang['ADMIN_SERVER_MEMBERLASTACTIVITY']); ?></th>
                <th><?php echo($lang['ADMIN_SERVER_MEMBERIP']); ?></th>
            </thead>
            <tbody>
                <?php
                    foreach ($online as $v) {
                        $user = $v["user"];
                        if ($v['time_start'] == 0)
                            $v['time_start'] = $v["time_lastactivity"];
                        $time_start = strftime("%d %b %Y %H:%M:%S", $v["time_start"]);
                        $time_lastactivity = strftime("%d %b %Y %H:%M:%S", $v["time_lastactivity"]);
                        $ip = $v["ip"];
                        $ogs = $v["ogs"] == 1 ? "(OGS)" : "";

                        echo "<tr>";
                            echo "<td>" . $user . " " . $ogs . "</td>";
                            echo "<td>" . $time_start . "</td>";
                            echo "<td>" . $time_lastactivity . "</td>";
                            echo "<td>" . $ip . "</td>";
                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>


